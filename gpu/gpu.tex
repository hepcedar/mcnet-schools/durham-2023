\documentclass[10pt]{article}

\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{helvet}
\usepackage{url}
\usepackage{mciteplus}
\usepackage[dvipsnames]{xcolor}
\usepackage{minted}
\usepackage{hyperref}

\newif\ifsolutionenabled\solutionenabledfalse
\newenvironment{solution}{\vspace{1.0em}\noindent\textcolor{RubineRed}{\textbf{Solution:}}}{}

\begin{document}

\title{{\Huge\bf Computing on GPUs}}
\author{{\Large Tutorial}}
\date{MCnet Summer School 2023}
\maketitle

\ifsolutionenabled
\begin{solution}
  Note that this is the version
  uploaded after the tutorial,
  which has embedded solutions.
  The results have been produced
  on different hardware than the one
  made available to the students,
  so don't expect to see the same
  performance measures
  (but the qualitative conclusions should
  be the same).
  The solutions refer to a ``tutor version''
  of the code,
  which includes all necessary changes
  to solve the exercises.
  Note that this is not publicly available,
  but please feel free to
  contact me (\href{enrico.bothmann@uni-goettingen.de})
  to obtain a copy or a snippet for a given exercise.
\end{solution}
\fi

\section{Prerequisites}

For this tutorial, each one of you has been provided
with an \texttt{ssh} access
to their own cloud-based GPU-equipped virtual machine.
Open a shell on your laptop,
and use the \texttt{ssh} command to open a remote
shell for that machine:
%
\begin{verbatim}
  ssh <username>@<address>
\end{verbatim}
%
When prompted for the password, type in the one that has been
given to you along with your username and address.
%
Next, 
you need a copy of the sample project
on your GPU machine:
%
\begin{verbatim}
  git clone https://gitlab.com/ebothmann/gpu-rambo-tutorial-student-version.git
  cd gpu-rambo-tutorial-student-version
\end{verbatim}
%
Now configure the project:
%
\begin{verbatim}
  cmake -S . -B build -DCMAKE_BUILD_TYPE=Release
\end{verbatim}
%
Make sure that the output reports that CUDA and Thrust have been found successfully.
The next step is to compile and run the project:
%
\begin{verbatim}
  cmake --build build -j 4
  build/src/rambo
\end{verbatim}
%
This will generate a million Rambo $2 \to 2$ points and write
histogram data for their distribution in the rapidity $y$
and the transverse momentum $p_T$
into the files \texttt{y.csv} and \texttt{pt.csv}, respectively.
This is all happening on the CPU for now,
but we will port part of this program to the GPU
in this tutorial.

\section{Rambo phase-space sampling}

Rambo is a simple phase-space generator for particle production at a collider~\cite{Kleiss:1985gy}.
It samples the $n$ massless particle phase space volume
\begin{equation}
  V_n(w)=\int \delta^4\left(P-\sum_{i=1}^n p_i\right) \prod_{i=1}^n\left(\text d^4 p_i \delta\left(p_i^2\right) \theta\left(p_i^0\right)\right)
\end{equation}
at a centre-of-mass energy $w$,
where the $p_i$ are the four-momenta of the particles.

This sampled distribution is flat in the $\text d^4 p_i$,
i.e.\ each point in the accessible phase-space has the same probability
(or ``phase-space weight'').
In itself, Rambo is therefore maximally efficient.
However, in an event generator the Monte-Carlo integrand would include
a squared matrix element, and possibly some parton distribution functions, which are in general not flat in phase space,
and might in particular feature several singular or near-singular structures.
Then, Rambo is indeed very inefficient and is usually only used as a last resort,
when there is no prior knowledge about the structure of the integrand,
or to validate more sophisticated phase-space generators.

In the provided sample code you will find an implementation of the Rambo algorithm
to sample momenta in the $2 \to n$ phasespace.
This will serve as an instructive toy example to learn about porting computational hotspots
of a program to run on the GPU.

\subsection{Study the distributions in $y$ and $p_T$}

The distribution of the outgoing momenta is histogrammed
in the transverse momentum $p_T$ and the rapidity $y$,
which are written to the files \texttt{pt.csv} and \texttt{y.csv}, respectively.
Open a second local shell on your laptop,
make a second local clone of the repository,
copy the results from the remote machine,
and plot them using the provided script:
\begin{verbatim}
  git clone https://gitlab.com/ebothmann/gpu-rambo-tutorial-student-version.git
  cd gpu-rambo-tutorial-student-version
  scp <username>@<address>:gpu-rambo-tutorial-student-version/{pt,y}.csv .
  python scripts/plot_histogram.py pt.csv y.csv
\end{verbatim}
This generates the files \texttt{pt.pdf} and \texttt{y.pdf}.
The plot script requires the \texttt{matplotlib} and \texttt{numpy} python modules.
If this is somehow not available, find a fellow student that has the packages,
so you can study the resulting histogram plots together.

Answer the following questions:
1) Are the distributions flat in $y$ and $p_T$? Why not?
2) Is the $y$ distribution biased towards central or forward production of outgoing particles? Why?
3) Is the $p_T$ distribution biased towards soft or hard production of outgoing particles? Why?
4) Given these distributions, do you think that Rambo is an efficient phase-space generator
for e.g.\ gluon scattering ($gg \to gg$)? Note that at fixed centre-of-mass energy
the scattering cross section diverges when the outgoing gluons become collinear
with the incoming gluons.

\ifsolutionenabled
\begin{solution}
  \begin{center}
    \includegraphics[width=0.49\linewidth]{pt.pdf}
    \hfill
    \includegraphics[width=0.49\linewidth]{y.pdf}
  \end{center}
  \begin{enumerate}
    \item
      It's not. Flat in dp\_i does not mean flat in y and p\_T, as can be seen by
      inspecting the respective eqs.\ (see code in \texttt{src/vec4.h})
    \item
      It's biased towards central production. This is a consequence of the
      logarithm in the eq. for y.
    \item
      It's strongly biased towards hard production. As we are biased towards
      central production, and we only generate one back-to-back pair, this is not
      surprising.
    \item
      No, the divergence would favour forward production
      (large $y$),
      the resulting overall distribution in $y$ is much flatter
      than the distribution sampled by Rambo.
  \end{enumerate}
\end{solution}
\fi

\subsection{Determine the hot spots of the program}
\label{sec:cpu_hot_spots}

Before we port anything to the GPU,
we should find the computational hot-spots of the program.

Go to \texttt{src/main.cpp} and add the include statement
\texttt{\#include "timing.h"} at the top.
This provides some convenience code to measure durations,
in particular the \texttt{Timer} class,
that can be used as in the following way:
\begin{minted}{cpp}
  double task_1_duration = 0.0;
  double task_2_duration = 0.0;
  Timer timer;
  // code for task 1 here
  task_1_duration += timer.elapsed();
  timer.reset();
  // code for task 2 here
  task_2_duration += timer.elapsed();

  // after the measurements are done:
  Timing::register_duration("Task 1 label", task_1_duration);
  Timing::register_duration("Task 2 label", task_2_duration);

  // add this line at the end of the program to print all registered durations
  Timing::print_durations(std::cerr);
\end{minted}
Add such timing code to measure the time required to
1) fill random numbers (\texttt{rng.fill(...)}),
2) fill momenta (\texttt{rambo.fill\_momenta(...)}),
3) fill phase-space weights (\texttt{rambo.fill\_weights(...)})
and to 4)~fill the two histograms (i.e.\ the whole loop around the \texttt{histo.fill(...)} calls).
Which of the tasks is the current bottleneck?
You should find that the \texttt{rambo.fill\_momenta(...)}
calls take up the most time.

\ifsolutionenabled
\begin{solution}
  Compile and run the tutor version with
  \begin{verbatim}
    cmake -S . -B build -DCMAKE_BUILD_TYPE=Release -DCUDA_DISABLED=1 -DTIMERS_ENABLED=1
    cmake --build build -j 12
    build/src/rambo
  \end{verbatim}
  The output (on cuda09) is
  \begin{verbatim}
    Task,                         Duration [s]
    Fill histograms,              0.495449
    Generate momenta,             0.657545
    Generate phase-space weights, 0.463673
    Generate random numbers,      0.437755
  \end{verbatim}
  We indeed find that generating the momenta currently
  takes up most of the overall computing time,
  although not by much!
  We will see below that batched event generation
  will already change this picture quite significantly \ldots
\end{solution}
\fi

\subsection{Prepare parallel computation of events}

To \emph{prepare} parallel evaluation
let us change the event generation such
that $n_\text{batch}$ events (= a batch of events) are generated
for a given iteration of the top-level \texttt{for}-loop,
instead of just a single event.
For example, in a given iteration, \texttt{rng.fill(...)}
will be used to generate random numbers for $n_\text{batch}$ events,
then \texttt{rambo.fill\_momenta(...)} is used
to generate the momenta for all $n_\text{batch}$ events, etc.
To achieve this, replace the top-level \texttt{for}-loop
over events in \texttt{src/main.cpp}
with a loop over $m$ event batches,
and increase the size of the \texttt{std::vector}'s
for random numbers, momenta, etc.\ such that they
can hold the data for a $n_\text{batch}$ events, instead of just one event.
Note that \texttt{rng.fill(...)} etc.\ are already
implemented such that they fill data for as much events
as storage is provided to them;
hence you should not need to change these implementations.
After finalising your changes, re-plot the histograms
to make sure that their shapes still look the same.
Also compare the new timings with the old ones.

\ifsolutionenabled
\begin{solution}
  Compile and run the tutor version with
  \begin{verbatim}
    cmake -S . -B build -DCMAKE_BUILD_TYPE=Release \
      -DCUDA_DISABLED=1 -DTIMERS_ENABLED=1 -DCPU_EVENT_BATCH_MODE_ENABLED=1
    cmake --build build -j 12
    build/src/rambo
  \end{verbatim}
  The output (on cuda09) is
  \begin{verbatim}
    Task,                         Duration [s]
    Fill histograms,              0.145239
    Generate momenta,             0.320143
    Generate phase-space weights, 0.00571386
    Generate random numbers,      0.08083
  \end{verbatim}
  Note that doing the same task for a whole batch,
  instead of going through the tasks for each event individually,
  already cuts down the time required substantially.
  While the calculations are the same,
  this approach improves the opportunities for using cached data.
  Note that generating the momenta profits least,
  making this task now stand out even more.
  Hence this is our target for optimisation.
\end{solution}
\fi

\subsection{Parallelise momentum generation using CUDA}

In the following, we will
move the implementation of the momentum generation
to the GPU.
First, we need to provide data storage
for the input (random numbers) and output data (momentum components)
\emph{on the device} (usually, the GPU RAM is distinct from the CPU RAM).
To do this, we use the Thrust library which provides some abstractions,
so we do not need to use lower-level C API to allocate and deallocate
memory.
Begin by including the header,
by adding
\texttt{\#include <thrust/device\_vector.h>}
at the top of \texttt{src/main.cpp}.
Now declare the variables \texttt{d\_r} and \texttt{d\_p}
of the type \texttt{thrust::device\_vector<double>}.
Initialise them to the same size as their host-side
counterparts \texttt{r} and \texttt{p}.
Then, after filling the random numbers,
do a host-to-device copy of these numbers:
\begin{minted}{cpp}
  thrust::copy(r.begin(), r.end(), d_r.begin());
\end{minted}
Do the same for the momentum components,
after filling the momenta,
this time copying from the device-side \texttt{dev\_p}
to the host-side \texttt{p}.
Only then will they be available for filling the histograms on the CPU.

With the momentum copies in place, we can now take care of porting the actual calculation.
For this, go to \texttt{src/rambo.h}
and add another \texttt{fill\_momenta} function declaration,
which takes \texttt{thrust::device\_vector}s as arguments
instead of \texttt{std::vector}s.
For this to compile,
you will also need
to include the required header;
add the line
\texttt{\#include <thrust/device\_vector.h>}
at the top of the file.
Then go to \texttt{src/rambo.cpp}
and define the implementation of the new function as follows:
\begin{minted}{cpp}
  void Massless_rambo::fill_momenta(thrust::device_vector<double> &p,
                                    thrust::device_vector<double> &r,
                                    double x1, double x2) const {
    const size_t batch_size{p.size() / 4 / n_ptcl};
    fill_momenta_kernel<<<batch_size / 256, 256>>>(
        thrust::raw_pointer_cast(&p[0]), thrust::raw_pointer_cast(&r[0]), x1, x2,
        n_ptcl, e_cms);
  }
\end{minted}
Study the implementation.
First, the batch size $n_\text{batch}$ is derived from the provided storage.
Then the special CUDA syntax \texttt{fill\_momenta\_kernel<<<...>>>} calls
a function (or kernel) that is executed on the GPU.
The degree of parallelisation is defined within the angled brackets,
with the first argument specifying the number of thread blocks
and the second argument specifying the number of threads per block.
The product of both numbers gives the total number of threads
that will execute the function.
Here, it is equal to $n_\text{batch}$, because we want to generate one event
per thread.
As for the arguments,
we use \texttt{thrust::raw\_pointer\_cast}
to pass along the memory address of the underlying storage of
\texttt{double}s,
and we pass \texttt{n\_ptcl} and \texttt{e\_cms},
because the \texttt{fill\_momenta\_kernel}
is not a member function of the \texttt{Massless\_rambo}
class and therefore has no direct access its
member variables.

Next, we need to define the device-side function,
\texttt{fill\_momenta\_kernel<<<...>>>}
called in the snippet above.
Add the definition somewhere above the new \texttt{fill\_momenta}
definition as follows:
\begin{minted}{cpp}
  __global__ void fill_momenta_kernel(double *p, double *r, double x1, double x2,
                                      int n_ptcl, double e_cms) {
    ...
  }
\end{minted}
Note the \texttt{\_\_global\_\_} keyword
which indicates that this function/kernel
will be compiled to run on the GPU.
To fill in the code for the dots ``\texttt{...}'',
copy the code from the old host-side \texttt{fill\_momenta}
function definition.
However, you must remove the loop over events
(because the device-side kernel will be run in parallel over all events
anyway),
and instead calculate the per-thread event index using
\begin{minted}{cpp}
  const size_t evt{threadIdx.x + blockDim.x * blockIdx.x};
\end{minted}
Moreover, you need to determine the batch size $n_\text{batch}$,
which in this context ist the total number of threads running
this kernel.
This is given by
\begin{minted}{cpp}
  const size_t batch_size{blockDim.x * gridDim.x};
\end{minted}
Note that \texttt{threadIdx}, \texttt{blockIdx},
\texttt{blockDim} and \texttt{gridDim}
are special variables that are defined by CUDA for you,
and contain the information about number of threads issued,
and which thread we are executing.
Make sure that the \texttt{Momentum\_indexer},
which is used to calculate the index
of a given momentum component in the common storage,
is updated to operate with \texttt{evt}.
You will also need to change the underlying data storage type
(as given via its \texttt{<...>} template parameter)
to \texttt{double*},
since we use a raw pointer on the device
instead of the \texttt{std::vector<double>}
that we used on the host.
This is all, you have ported the momentum generation to the GPU!
Go back to \texttt{src/main.cpp} and make sure
that you call the new \texttt{fill\_momenta},
passing \texttt{d\_r} and \texttt{d\_p}
instead of \texttt{r} and \texttt{p}.

Finally, change your batch size $n_\text{batch}$
and the number of batches $m$ you generate
to something that fit the new implementation.
You may have noticed that
our \texttt{fill\_momenta\_kernel<<<batch\_size / 256, 256>>>}
assumes that $n_\text{batch}$ is a multiple of 256.
We don't want to underutilise the GPU,
so let's start with a decent batch size $n_\text{batch}$,
say $n_\text{batch}=10240$.
The number of batches $m$ only matters in the sense
that we need enough statistics to generate smooth histograms
for testing.
You can set this number to $100$, such that about a million
events are generated in total,
just like on the CPU before.

Check if your changes compile and run without an error.

\ifsolutionenabled
\begin{solution}
  Edit \texttt{src/main.cpp}
  to use the suggested
  batch size of 10240 and number of batches of 100,
  as suggested.
  Compile and run the tutor version with
  \begin{verbatim}
    cmake -S . -B build -DCMAKE_BUILD_TYPE=Release \
      -DCUDA_DISABLED=0 -DTIMERS_ENABLED=1 -DCPU_EVENT_BATCH_MODE_ENABLED=1
    cmake --build build -j 12
    build/src/rambo
  \end{verbatim}
\end{solution}
\fi

\subsection{Testing and measuring}

Now is the time to 1) make sure that everything still works
and 2) measure the timing to see what we have achieved.

For 1), it should be enough to re-plot the histograms
and check that their shape looks fine still.

For 2), we can look at our timers.
However, first add two more timers that measure
how long the host-to-device and device-to-host
copies take.
Make sure that the timer that measures the filling
of momenta only measures the duration of the
\texttt{fill\_momenta} call.
Check if adding the following line,
\begin{minted}{cpp}
  CUDA_CALL(cudaDeviceSynchronize());
\end{minted}
right after the
\texttt{fill\_momenta} call,
and before you measure its duration,
makes any difference.
The reason is that CUDA device-side function calls
via the \texttt{kernel<<<...>>>(...)} syntax
are issued asynchronously, i.e.\ control is handed
back to the CPU immediately, without waiting for
the parallel execution to conclude.
The synchronisation call enforces that we wait
for all GPU tasks to finish before continuing,
which might be relevant for adding the duration
to the correct timer.
Now check the results.
Did we remove the previous bottleneck
of CPU-only execution?
How much faster is the GPU compared to the CPU
for this task?
Is the time needed for the copies significant?
Vary the batch size up and down by a factor of ten,
respectively (adjusting the number of batches $m$ accordingly),
and check if there is any difference.
If varying up gives you better performance,
the GPU was not yet fully utilised.

\ifsolutionenabled
\begin{solution}
  The new pull/push timers and the CUDA synchronisation call
  are already included in the version compiled in the last subsection.
  So we can simply run. The output (on cuda09) is:
  \begin{verbatim}
    Task,                         Duration [s]
    Fill histograms,              0.148629
    Generate momenta,             0.00286806
    Generate phase-space weights, 0.000440384
    Generate random numbers,      0.171506
    Pull momenta,                 0.0210527
    Push random numbers,          0.0184204
  \end{verbatim}
  We did certainly remove the momentum generation bottleneck,
  which now takes about a 100th of the time we measured for the CPU.
  While the momentum copies only take about 10\,\% of the runtime,
  they are not insignificant,
  especially when comparing to the actual calculation on the GPU,
  which only takes about 5\,\% of the time needed to copy
  the input and the output of the kernel!

  Varying the batch size up by a factor of then
  ($n_\text{batch}=102400$, number of batches = 10):
  \begin{verbatim}
    Task,                         Duration [s]
    Fill histograms,              0.140584
    Generate momenta,             0.00188719
    Generate phase-space weights, 0.000865997
    Generate random numbers,      0.174444
    Pull momenta,                 0.0135197
    Push random numbers,          0.0136541
  \end{verbatim}
  This reduces all GPU-related tasks,
  both the kernel runtime
  and the copy times,
  each by about $30\,\%$.
  This indicates that the GPU was under-utilised before
  (and it might still be).

  Reducing the batch size by a factor of ten (to $n_\text{batch}=1024$),
  on the other hand,
  reduces performance significantly,
  in particular the kernel runtime, which is now ten times longer:
  \begin{verbatim}
    Task,                         Duration [s]
    Fill histograms,              0.13584
    Generate momenta,             0.0245014
    Generate phase-space weights, 0.000814115
    Generate random numbers,      0.167227
    Pull momenta,                 0.0320783
    Push random numbers,          0.0203666
  \end{verbatim}
  This indicates that we don't use all of the available threads.
\end{solution}
\fi

\subsection{Profiling for beginners}

CUDA provides several command-line tools
(and also graphical application)
to understand the utilisation of GPU better
and even give suggestions for improving it.
The first high-level overview of what
the GPU is used for and what CUDA is doing on the CPU
can be generated by running your application with the \texttt{nvprof} tool: \begin{verbatim}
  nvprof build/src/rambo
\end{verbatim}
Under the header \texttt{GPU activities}
you can see a list of tasks done on the device
ordered by the total time needed.
You might find that this is dominated by
the task \texttt{[CUDA memcpy DtoH]},
meaning that copying the momenta takes up a lot of time.
Under the header \texttt{API calls}
you see a list of CUDA-related tasks that happen on the CPU,
such as \texttt{cudaFree}, which deallocated memory,
or \texttt{cudaLaunchKernel}, which gives the overhead
of calling a function (= kernel) that is run on the device.
Part of that overhead is copying the arguments to the device.
If this is large, you might want to reduce the number of kernels
executed, e.g.\ by combining several small kernels into one larger kernel.

The second tool we want to discuss here is \texttt{Nsight Compute}.
On the command line, you can call it like this:
\begin{verbatim}
  sudo /usr/local/cuda-12.2/bin/ncu --set full build/src/rambo
\end{verbatim}
This might take considerable time.
You can minimise it by only generating a single batch of events,
i.e.\ setting $m=1$.
A lot of output is generated for each kernel.
Search for \texttt{fill\_momenta\_kernel}
to find information pertaining to that kernel.
You will find several sections,
and in most of them some
paragraphs introduced with \texttt{OPT}
which suggest options to improve performance.
Scroll to the \texttt{Source Counters} section
(it should appear at the end of the output)
and read the \texttt{OPT} below it.
It should point out that there are many
uncoalesced memory accesses,
i.e.\ the data accessed by thread $i$
is not always directly adjacent
to the data accessed by thread $i+1$.
As discussed in the lecture,
this prevents the GPU to combine several reads/writes from/to its
RAM into a single read/write operation.
To fix this for accessing the momentum data,
go to \texttt{indexing.h},
where you will find that the accesses
are ordered hierarchically like this:
\texttt{event index -> particle index -> momentum component}.
How do you need to change this for coalesced memory accesses?
Modify the code accordingly and re-generate
the \texttt{ncu} output.
Did the number of uncoalesced memory accesses go down?
Do another normal run to check how this affects
the performance of the \texttt{fill\_momenta\_kernel}.
You can also implement this change in your CPU version
and check if it impacts the performance \emph{negatively}
for this hardware.

\ifsolutionenabled
\begin{solution}
  In the following, we will keep using
  the best-performing batch size of $n_\text{batch}=102400$.
  The \texttt{nvprof GPU activities} output is
  \begin{verbatim}
    Time(%)      Time     Calls       Avg       Min       Max  Name
    48.25%  12.118ms        10  1.2118ms  1.1923ms  1.2928ms  [CUDA memcpy DtoH]
    44.76%  11.243ms        10  1.1243ms  1.0827ms  1.3269ms  [CUDA memcpy HtoD]
     6.82%  1.7141ms        10  171.41us  164.38us  181.89us  fill_momenta_kernel(...)
     0.16%  41.248us         3  13.749us  4.6080us  24.288us  void thrust::cuda_cub::...
  \end{verbatim}
  Also here one can see how dominating the memory copies are.
  The actual calculation only takes 6.82\,\% percent of the GPU time.
  And the (top lines of the) \texttt{nvprof API calls} are
  \begin{verbatim}
    87.54%  209.58ms         3  69.860ms  90.341us  209.37ms  cudaMalloc
    10.88%  26.054ms        20  1.3027ms  1.2236ms  1.5610ms  cudaMemcpyAsync
     0.73%  1.7382ms        10  173.82us  167.17us  184.72us  cudaDeviceSynchronize
     0.35%  829.55us         3  276.52us  135.32us  536.52us  cudaFree
     0.29%  693.55us        23  30.154us  1.6720us  64.491us  cudaStreamSynchronize
     0.08%  197.26us        13  15.173us  9.8250us  56.401us  cudaLaunchKernel
  \end{verbatim}
  One can see that allocating GPU memory (\texttt{cudaMalloc})
  takes a lot of time. However, this is only done once
  for each \texttt{device\_vector},
  and therefore does not scale with the statistics we generate.
  Hence, in a realistic use case, this startup time would
  not longer play a role.

  Now let's proceed with \texttt{ncu}.
  First we set the number of batches in \texttt{src/main.cpp} to 1,
  as suggested, and re-compile.
  We look through the output as suggested
  and find the following warning about uncoalesced memory accesses:
  \begin{verbatim}
    WRN   This kernel has uncoalesced global accesses resulting in a total of
          4147200 excessive sectors (73% of the total 5683200 sectors). Check
          the L2 Theoretical Sectors Global Excessive table for the primary
          source locations. The CUDA Programming Guide had additional
          information on reducing uncoalesced device memory accesses.
  \end{verbatim}

  To fix this, we recompile with `-DEVENT\_MINOR\_INDEXING\_ENABLED=1`.
  This changes the indexing hierarchy to
  \texttt{particle index -> momentum component -> event index},
  i.e.\ moving the event index to the last position,
  thus ensuring coalescent memory accesses.
  Rerunning the profiling now reports only 26\,\% excessive sectors.
  With that our timing output looks as follows:
  \begin{verbatim}
    Task,                         Duration [s]
    Fill histograms,              0.136339
    Generate momenta,             0.000661525
    Generate phase-space weights, 0.000717533
    Generate random numbers,      0.176543
    Pull momenta,                 0.0135467
    Push random numbers,          0.0136846
  \end{verbatim}
  Indeed, the momentum generation time
  is reduced by a factor of three
  just by ensuring coalescent memory accesses!
  Note that we did not fix the access of the random numbers,
  which explains the remaining uncoalesced accesses reported.
\end{solution}
\fi

\section{Advanced exercises}

If you managed to get through the above material in time,
you can either help your fellow students,
or you can work through the following advanced tasks.
There do not depend on each other, so you can pick the one that interests you most.
As these are advanced exercises,
there are no detailed step-by-step instructions.

\subsection{Port random number generation to the GPU}
\label{sec:rng}

Random number generation can also be parallelised.
Fortunately, the \texttt{curand} library provided
with CUDA offers everything we need,
and the necessary calls are already
part of \texttt{src/rng.h} and \texttt{src/rng.cpp}.
Study the CUDA-specific code in these files,
which are only compiled
if \texttt{DEVICE\_RNG\_ENABLED} is set.
This can be done using
\texttt{cmake -S . -B build -DDEVICE\_RNG\_ENABLED=1}.
Now modify \texttt{src/main.cpp}
to generate the random numbers directly on the GPU.
Recompile your code and study the timing output.
You can also run with \texttt{nvprof} to get
additional information about
the random number generation on the GPU.

\subsection{Port filling the phase-space weights to the GPU}
\label{sec:wgt}

Also port the filling of the phase-space weights to run on the GPU.
Currently, this is done on the CPU, using \texttt{Massless\_rambo::fill\_weights}.
You can adapt the steps needed to port the \texttt{fill\_momenta}
calculation.
Make sure to copy back the device-side weights data to the host,
such that it is available when filling the histograms.

\subsection{Port filling the histograms to the GPU}

In case have already finished tasks~\ref{sec:rng} and~\ref{sec:wgt},
the only substantial step still done on the CPU
is filling the histograms.
For this, we can use the Thrust API.
Port the code example provided at the following URL:
\begin{verbatim}
  https://github.com/NVIDIA/thrust/blob/master/examples/histogram.cu
\end{verbatim}
Note that you might want to remove now obsolete device-to-host data copying
after porting the histogram filling.

If you want to know about the low-level complexities
of implementing the binning of data, read the following blog post
by NVidia:
\begin{verbatim}
  https://developer.nvidia.com/blog/gpu-pro-tip-fast-histograms-using-shared-atomics-maxwell/
\end{verbatim}
In particular, binning data is a task where threads need to cooperate to fill
a shared data structure (the histogram bins).
If this is done wrongly, race conditions can occur when several threads
update a bin simultaneously.
This is different to the other tasks in this tutorial,
where each thread was completely independent of all other threads.

\subsection{Speed-ups vs.\ multiplicity}

Plot the GPU-vs-CPU speed-ups of 1) the \texttt{fill\_momenta\_kernel}
and 2) the whole program, varying the final-state multiplicity~$n$
on the $x$-axis.
Note that you might need to adapt the batch size $n_\text{batch}$ for each multiplicity,
since you might need smaller ones for larger multiplicities.

\section{Further reading}
\begin{itemize}
  \item
    \href{https://enccs.github.io/gpu-programming/}{GPU Programming: When, Why and How?}
  \item
    \href{https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html}{CUDA C++ Programming Guide}
\end{itemize}

\bibliographystyle{amsunsrt_modp}
\bibliography{gpu}

\end{document}
