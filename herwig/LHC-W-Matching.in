# -*- ThePEG-repository -*-

##################################################
## Herwig/Matchbox example input file
##################################################

##################################################
## Collider type
##################################################
read snippets/Matchbox.in
read snippets/PPCollider.in

set /Herwig/Samplers/CellGridSampler:InitialPoints 5000

##################################################
## Beam energy sqrt(s)
##################################################

cd /Herwig/EventHandlers
set EventHandler:LuminosityFunction:Energy 13000*GeV
set EventHandler:Weighted Yes

# disable hadronisation, decays, MPI
# set EventHandler:HadronizationHandler NULL
# set EventHandler:DecayHandler NULL
# set EventHandler:CascadeHandler:MPIHandler NULL
# set EventHandler:MultipleInteractionHandler NULL
# set EventHandler:DecayHandler NULL
# set /Herwig/Analysis/Basics:CheckQuark No

##################################################
## Process selection
##################################################

## Note that event generation may fail if no matching matrix element has
## been found.  Coupling orders are with respect to the Born process,
## i.e. NLO QCD does not require an additional power of alphas.

## Model assumptions
read Matchbox/StandardModelLike.in
read Matchbox/DiagonalCKM.in

## Set the order of the couplings
cd /Herwig/MatrixElements/Matchbox
set Factory:OrderInAlphaS 0
set Factory:OrderInAlphaEW 2

## Select the process
## You may use identifiers such as p, pbar, j, l, mu+, h0 etc.

do Factory:Process p p -> l nu

##################################################
## Matrix element library selection
##################################################

## Select a generic tree/loop combination or a
## specialized NLO package

# read Matchbox/MadGraph-MadGraph.in
# read Matchbox/MadGraph-OpenLoops.in

##################################################
## Set W mass
##################################################
read MassW.in
##################################################
## Cut selection
## See the documentation for more options
##################################################
cd /Herwig/Cuts/
insert Cuts:TwoCuts 0 LeptonPairMassCut
set LeptonPairMassCut:MinMass 1*GeV
#set LeptonPairMassCut:MaxMass 120*GeV

##################################################
## Scale choice
## See the documentation for more options
##################################################

cd /Herwig/MatrixElements/Matchbox
set Factory:ScaleChoice /Herwig/MatrixElements/Matchbox/Scales/LeptonPairMassScale

##################################################
## Matching and shower selection
## Please also see flavour scheme settings
## towards the end of the input file.
##################################################

# read Matchbox/MCatNLO-DefaultShower.in
# read Matchbox/Powheg-DefaultShower.in
## use for strict LO/NLO comparisons
# read Matchbox/MCatLO-DefaultShower.in
## use for improved LO showering
# read Matchbox/LO-DefaultShower.in

## Dipole Shower choices
read Matchbox/MCatNLO-DipoleShower.in
# read Matchbox/Powheg-DipoleShower.in
## use for strict LO/NLO comparisons
# read Matchbox/MCatLO-DipoleShower.in
## use for improved LO showering
# read Matchbox/LO-DipoleShower.in

# read Matchbox/NLO-NoShower.in
# read Matchbox/LO-NoShower.in

##################################################
## Scale uncertainties
##################################################

# read Matchbox/MuDown.in
# read Matchbox/MuUp.in

##################################################
## Shower scale uncertainties
##################################################

# read Matchbox/MuQDown.in
# read Matchbox/MuQUp.in

##################################################
## PDF choice
##################################################

# Flavour scheme

# read Matchbox/FiveFlavourScheme.in
## required for dipole shower and fixed order in five flavour scheme
read Matchbox/FiveFlavourNoBMassScheme.in


##################################################
## PDF choice
##################################################

# read Matchbox/CT14.in
# read Matchbox/MMHT2014.in

cd /Herwig/Partons

# you can use any installed LHAPDF set here:
# e.g. CT18NLO, NNPDF40_nlo_as_01180, MSHT20nlo_as118, PDF4LHC21_40

set HardLOPDF:PDFName NNPDF30_nlo_as_0118
set HardNLOPDF:PDFName NNPDF30_nlo_as_0118
set ShowerLOPDF:PDFName NNPDF30_nlo_as_0118
set ShowerNLOPDF:PDFName NNPDF30_nlo_as_0118
set MPIPDF:PDFName NNPDF30_nlo_as_0118
set RemnantPDF:PDFName NNPDF30_nlo_as_0118


##################################################
## Analyses
##################################################

cd /Herwig/Analysis
insert /Herwig/Generators/EventGenerator:AnalysisHandlers 0 Rivet

## add here your Rivet analyses:
insert Rivet:Analyses 0 MY_W_ANALYSIS

#insert Rivet:Analyses 0 MC_WINC
#insert Rivet:Analyses 0 MC_WJETS
#insert Rivet:Analyses 0 MC_XS


##################################################
## Save the generator
##################################################

do /Herwig/MatrixElements/Matchbox/Factory:ProductionMode
set /Herwig/Generators/EventGenerator:IntermediateOutput Yes

cd /Herwig/Generators
## saves a LHC-W-production.run file in the current working directory
## from which Herwig is executed
saverun LHC-W-Matching EventGenerator
