# Herwig Tutorial
As a warm-up you can do the tutorial [here](https://phab.hepforge.org/w/herwigtutorial/) 
Note:
- The Docker tutorial calls for creating an alias *DRH*, but alternatively you can also use the script `./run_container.sh`, which gives you a shell inside the image and linking your local filesystem to it
- If you use the `run_container.sh` script you may need to give it execution permissions by the command `chmod +x run_container.sh` once before running it
- NOTE: if you already pulled the docker image yesterday before Sunday 9 July 12:10 you may want to pull again since there has been an update

## MONDAY

Contact: Stefan Gieseke, Stefan Kiebacher

The Herwig tutorial webpage can be found here [Herwig tutorial](https://phab.hepforge.org/w/herwigtutorial/)

Advanced tutorials and instructions for the installation can be found here [Herwig webpage](https://herwig.hepforge.org/tutorials/index.html)

