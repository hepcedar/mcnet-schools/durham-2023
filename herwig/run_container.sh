#!/bin/bash

## README:
## you may need to run once "chmod +x run_container.sh" to give permissions
## to execute this script. If you are using mac uncomment the line after "Mac:"
## at the bottom and comment the line after "Linux:"
## Run this script as "./run_container.sh" to get a shell inside the container
## and link your current working directory to it
## WARNING: for linux users you need to run docker with root priveledges (see below)
##			so be careful and think before typing!


## Official herwig docker image:
CONTAINER="herwigcollaboration/herwig-7.3"
VER="7.3.0"
## Alternative rivet-herwig docker image
# CONTAINER="hepstore/rivet-herwig"
# VER="latest" # latest Herwig in rivet-herwig repo is 7.2.3 only!!!

## Mac:
# docker container run -it -v $PWD:$PWD -w $PWD $CONTAINER:$VER /bin/bash
## Linux:
sudo docker container run -it -v $PWD:$PWD -w $PWD $CONTAINER:$VER /bin/bash
