# Rivet tutorial: Analysis prototyping and multiweights

## TUESDAY

Contact: Christian Gutschow

**NB: It would be advisable to follow the prerequisites (below) ahead of the tutorial to avoid bandwidth issues.**


## Prerequisites


For this tutorial, we will be using the latest Rivet Docker image. To download the image, simply type:

```
  docker pull hepstore/rivet:3.1.8
```

You can run the container interactively, e.g. like so

```
  docker container run -it -v $PWD:$PWD -w $PWD hepstore/rivet:3.1.8 /bin/bash
```
where the `-v $PWD:$PWD -w $PWD` ensures that you have access to your current directory also from within the container.
To check that it works, try e.g.

```
  rivet --version
```

which should return `rivet v3.1.8`.


In addition, we've prepared some HepMC event files for you to analyse.
It's advisable to download these ahead of the tutorial as well, which you can
download and untar as follows

```
   wget "https://rivetval.web.cern.ch/rivetval/TUTORIAL/truth-analysis.tar.gz" -O- | tar -xz --no-same-owner
```

You should see two files `Wjets13TeV_10k.hepmc.gz` and `Zjets13TeV_10k.hepmc.gz`
in your local directory, corresponding to particle-level $`W`$+jets and $`Z`$+jets events in
proton-proton collisons at a centre-of-mass energy of 13 TeV.


